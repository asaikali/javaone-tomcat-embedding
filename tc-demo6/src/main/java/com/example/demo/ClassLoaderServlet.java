package com.example.demo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


public class ClassLoaderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain");
		resp.getWriter().write(ClassLoaderUtil.getClassLoaderHireachy(ClassLoaderServlet.class));
		resp.getWriter().write(ClassLoaderUtil.getClassLoaderHireachy(HttpServlet.class));
		resp.getWriter().write(ClassLoaderUtil.currentThreadClassLoader());	
		
		try {
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/evctools");
			Connection connection = ds.getConnection();
			connection.createStatement().execute("SELECT 1");
			System.out.println("\nGot a database connection \n");
		} catch (NamingException | SQLException e) {
			e.printStackTrace();
		}
		
	}
}
