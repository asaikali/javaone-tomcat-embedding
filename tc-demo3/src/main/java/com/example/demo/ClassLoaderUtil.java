package com.example.demo;

public class ClassLoaderUtil {

	public static String currentThreadClassLoader() {
		ClassLoader contextLoader = Thread.currentThread()
				.getContextClassLoader();
		String result = "\n************************************************";
		result += "\nThread " + Thread.currentThread().getName()
				+ " ContextLoader is: " + contextLoader;
		return result;
	}

	public static String getClassLoaderHireachy(Class<? extends Object> clazz) {
		ClassLoader loader = clazz.getClassLoader();
		String result = "\n************************************************";
		result += "\n" + clazz.getCanonicalName() + " loaded by " + loader;
		result += "\nParent loaders: ";
		String indent = "  ";
		loader = loader.getParent();
		while (loader != null) {
			result += "\n" + indent + " Discovered Loader " + loader;
			loader = loader.getParent();
			indent += " ";
		}

		result += "\n";
		return result;
	}
}
