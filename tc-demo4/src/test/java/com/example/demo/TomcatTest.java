package com.example.demo;

import java.io.File;

import javax.servlet.ServletException;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class TomcatTest 
{	
	private static Tomcat tomcat; 
	
	@BeforeClass
	public static void startTomcat() throws ServletException, LifecycleException
	{
		String webappDirLocation = new File("src/main/webapp/").getAbsolutePath();
		tomcat = new Tomcat();
		tomcat.setPort(8080);
		tomcat.addWebapp("/", webappDirLocation );
		tomcat.start();
	}
	
	@AfterClass
	public static void stopTomcat() throws LifecycleException 
	{
		tomcat.stop();
	}
	
	@Test
	public void exampleTest()
	{
		System.out.println();
		System.out.println("put code to send requests to tomcat here.");
		System.out.println("line 1");
		System.out.println("line 2");
		System.out.println("line 3");
		System.out.println("line 4");
		Assert.assertTrue(false); // fail for demo purposes 
		System.out.println("line 5");
		System.out.println("line 6");
		System.out.println("line 7");
		System.out.println("line 8");
		System.out.println("line 9");
		System.out.println("line 10");
		System.out.println("line 11");
		System.out.println();
	}
	
}
