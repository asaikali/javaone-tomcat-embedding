package com.example.demo;
import java.io.File;

import org.apache.catalina.startup.Tomcat;

public class TomcatDemo2 {
	public static void main(String[] args) throws Exception {

		System.out.println("\nDemo App Version 3.0 - Development \n");
		String webappDirLocation = new File("src/main/webapp/").getAbsolutePath();
		Tomcat tomcat = new Tomcat();
		tomcat.setPort(8080);
		tomcat.addWebapp("/", webappDirLocation );
		System.out.println("Application started on Port 8080");
		System.out.println("Application path is " + webappDirLocation);

		tomcat.start();
		tomcat.getServer().await();
	}
}
