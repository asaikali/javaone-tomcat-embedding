## Teach Your Tomcat Embedding Tricks

Sept 23 2013 Java One BOF 

This presentation will provide you with an excellent introduction to different ways you can embedded Tomcat 7.0.x, along with some tricks for a more productive Tomcat, git & maven workflow. The presentation is a sequence of demos, source code and slides will be available on bitbucket. No previous Tomcat experience required.