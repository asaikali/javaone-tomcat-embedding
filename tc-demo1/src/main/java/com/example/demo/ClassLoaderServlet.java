package com.example.demo;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ClassLoaderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain");
		resp.getWriter().write(ClassLoaderUtil.getClassLoaderHireachy(ClassLoaderServlet.class));
		resp.getWriter().write(ClassLoaderUtil.getClassLoaderHireachy(HttpServlet.class));
		resp.getWriter().write(ClassLoaderUtil.currentThreadClassLoader());	
		
		resp.getWriter().write("\nNotice that the ClassLoaderServlet and the HttpServlet were loaded by the same class loader.");	
	}
}
